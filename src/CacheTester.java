import java.util.ArrayList;


public class CacheTester implements Runnable{

	Cache instance;
	private Thread thread = new Thread(this);
	private long testTime;
	@Override
	public void run() {
		ArrayList<SystemEnumeration> sys = new ArrayList<SystemEnumeration>();
		while (true)
		{
		sys = instance.get("DDOS");
		consolePrintResult(sys);
		try {
			Thread.sleep(testTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		}
		
	}
	public void start(Cache instace){
		this.instance=instace;
		thread.start();
	}
	public void setTestTime(long testTime){
		this.testTime = testTime;
	}
	public void consolePrintResult(ArrayList<SystemEnumeration> res){
		if(res==null) System.out.println("==Cache Tester BRAK DANYCH");
		if(res!=null){
			System.out.println("==Cache Tester :");
			for (int i=0; i<res.size(); i++){
				System.out.println("==" + res.get(i).getCode() + " " + res.get(i).getValue() + " " + res.get(i).getEnumerationName());
			}
		}		
	}

}
