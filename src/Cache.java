import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Cache {

	private static Cache instance;
	private HashMap<String, ArrayList<SystemEnumeration>> cacheObjects = new HashMap<String, ArrayList<SystemEnumeration>>();
	private HashMap<String, ArrayList<SystemEnumeration>> cacheUpdated = new HashMap<String, ArrayList<SystemEnumeration>>();
	
	private Cache(){		
	}
	public static Cache getInstance(){
		if (instance==null) 
			instance = new Cache();
		return instance;
	}
	public void clean(){
		cacheUpdated.clear();
	}
	public void put(SystemEnumeration element){
		if (! (cacheUpdated.containsKey(element.getEnumerationName()))){
			cacheUpdated.put(element.getEnumerationName(), new ArrayList<SystemEnumeration>());
		}
		cacheUpdated.get(element.getEnumerationName()).add(element);
	}
	public void finalizeUpdate(){
		cacheObjects = cacheUpdated;
	}	
	public ArrayList<SystemEnumeration> get(String key){
		return cacheObjects.get(key);
	}
}