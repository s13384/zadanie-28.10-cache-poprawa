
import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {
	
		Cache cache = Cache.getInstance();
		ArrayList<SystemEnumeration> latestData = new ArrayList<SystemEnumeration>();
		latestData.add(new SystemEnumeration(1101, 1, "POM", "woj pomorskie", "REGION"));
		latestData.add(new SystemEnumeration(1102, 2, "KPOM", "woj kujawsko-pomorskie", "REGION"));
		latestData.add(new SystemEnumeration(2801, 1, "KOM", "tel komorkowy", "PHONE-TYPE"));
		latestData.add(new SystemEnumeration(2801, 2, "ST", "tel stacjonarny", "PHONE-TYPE"));
		latestData.add(new SystemEnumeration(3001, 1, "DSA", "ddos strong attack", "DDOS"));
		latestData.add(new SystemEnumeration(3002, 2, "DA", "ddos attack", "DDOS"));
		Updater updater = new Updater();
		updater.setLifespan(10000);
		updater.setCacheInstance(Cache.getInstance());
		updater.setLatestData(latestData);
		updater.start();
		System.out.println("+++++++");
		CacheTester test = new CacheTester();
		test.setTestTime(1000);
		test.start(Cache.getInstance());
		
	}

}