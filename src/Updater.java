import java.util.ArrayList;


public class Updater implements Runnable {

	private Thread thread = new Thread(this);
	private long lifespan;
	private Cache cacheInstance;
	private ArrayList<SystemEnumeration> latestData;
	
	@Override
	public void run() {
		while (true && cacheInstance!=null)
		{
			try {
				refreshCache();
				consolePrintNewestData();
				Thread.sleep(lifespan);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	public void refreshCache(){
		cacheInstance.clean();
		try {
			Thread.sleep(2000);
			for (SystemEnumeration sysEnumer : latestData){
				cacheInstance.put(sysEnumer);
			}
			cacheInstance.finalizeUpdate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}
	public long getLifespan() {
		return lifespan;
	}

	public void setLifespan(long lifespan) {
		this.lifespan = lifespan;
	}

	public Cache getCacheInstance() {
		return cacheInstance;
	}

	public void setCacheInstance(Cache cacheInstance) {
		this.cacheInstance = cacheInstance;
	}
	public ArrayList<SystemEnumeration> getLatestData() {
		return latestData;
	}
	public void setLatestData(ArrayList<SystemEnumeration> latestData) {
		this.latestData = latestData;
	}
	public void start(){
		thread.start();
	}
	public void consolePrintNewestData(){
		System.out.println(">>Najnowsze dane :");
		for (int i=0; i<latestData.size(); i++){
			System.out.println(">>" + latestData.get(i).getCode() + " " + latestData.get(i).getValue() + " " + latestData.get(i).getEnumerationName());
		}
	}

	
	
}